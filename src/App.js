import React from 'react'
import SokoApp from './SokoApp'

function App() {
  return (
    <div>
      <SokoApp />
    </div>
  )
}

export default App
