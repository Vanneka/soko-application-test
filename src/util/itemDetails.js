const itemDetails = {
    details : [
        {
            itemName: 'kids 2pk cloth face mask',
            discount: false,
            discountNumber: null,
            itemPrice: '185000',
            itemImage: 'facemasks.jpg'
        },
        {
            itemName: 'norbury scandanavian wood leg ottoman',
            discount: true,
            discountNumber: '20',
            itemPrice: '148400',
            discountPrice: '',
            itemImage: 'scandanavian_chair.jpg'
        },
        {
            itemName: 'baby-cut carrots 1lb - good % gather',
            discount: false,
            discountNumber: null,
            itemPrice: '185000',
            discountPrice: '',
            itemImage: 'baby_carrots.jpg'
        },
        {
            itemName: 'metal country chalet charm and wood bench - saracina home',
            discount: true,
            discountNumber: '40',
            itemPrice: '185000',
            discountPrice: '',
            itemImage: 'ottoman.jpg'
        },
        {
            itemName: 'alternative protein ground - 16oz - good & gather',
            discount: false,
            discountNumber: null,
            itemPrice: '8,000',
            discountPrice: '',
            itemImage: 'ground_beef.jpg'
        },
    ]
}
export default itemDetails
