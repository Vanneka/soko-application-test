const menuList = {
    menu : [
        {
            menuName: 'electronics',
            numberOfItems: '12'
        },
        {
            menuName: 'face masks',
            numberOfItems: '3'
        },
        {
            menuName: 'fresh food',
            numberOfItems: '8'
        },
        {
            menuName: 'grocery',
            numberOfItems: '6'
        },
        {
            menuName: 'home',
            numberOfItems: '24'
        },
        {
            menuName: 'kids',
            numberOfItems: '9'
        },
    ]
}
export default menuList
