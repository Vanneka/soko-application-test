import React from 'react'

function Form() {
    const handleSubmit = (e)=>{
        e.preventDefault()
    }
    return (
        <div className="searchBar">
            <form action="">
                <input type="text" name="" id="" placeholder="Search for products..." />
                <button
                onClick = {(e)=>{handleSubmit(e)}}
                type="submit"
                ></button>
            </form>
        </div>
    )
}

export default Form
