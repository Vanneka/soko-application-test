import React from 'react'
import menuItems from '../util/menuList'

function Menu() {
    return (
        <div className="menuContainer" id="appItem">
            {
                menuItems.menu.map((menuItem, index) => {
                    return (
                        <div className='categoryName' key={index}>
                            <a href='/'>{menuItem.menuName} ({menuItem.numberOfItems})</a>
                        </div>
                    )
                })
            }
            <div className="displayCategories">
                <a href="/">View all categories</a>
            </div>

        </div>
    )
}

export default Menu
