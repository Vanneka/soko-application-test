import React from 'react'

function SectionTitle(props) {
    return (
        <div  className="sectionHeading">
            <h2 className="sectionTitle">{props.titleName}</h2>
            <p className="numberOfItems">
                {props.numberOfItems}
            </p>
        </div>
    )
}

export default SectionTitle
