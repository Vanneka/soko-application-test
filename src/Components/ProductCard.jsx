import React from 'react'

function ProductCard(props) {

    const DiscountCalculator = () => {
        let initialPrice = parseInt(props.itemDetails.itemPrice)
        let discountedAmt = parseInt(props.itemDetails.discountNumber) / 100 * initialPrice
        let discountedPrice = initialPrice - discountedAmt
        return (
            <p>UGX {discountedPrice}</p>
        )
    }

    return (
        <div className='productCard'>
            <div className="productImage">
                <img src={`images/${props.itemDetails.itemImage}`} alt="" />
            </div>
            <div className="productDetails">
                <p className="productName">{props.itemDetails.itemName}</p>
                {props.itemDetails.discount === true ?
                    <div>
                        <div  className="productPrice" ><DiscountCalculator/></div>
                        <p className="discountedPrice">UGX {props.itemDetails.itemPrice}</p>
                        <p className="displayDiscount">{props.itemDetails.discountNumber}%</p>
                    </div>
                    :  <p className="productPrice">UGX {props.itemDetails.itemPrice}</p>
                }
            </div>
            <div className="addBtn">
                <a href="/">+ add</a>
            </div>
        </div>
    )
}

export default ProductCard
