import React from 'react'

function Header() {
    return (
        <div className='headerSection'>
            <div className="storeMarker">
                <p>Store made with SOKO</p>
            </div>
            <div className="headerContent">
                <div className="headerIcon">
                    <img src="/images/header_icon.png" alt="" />
                </div>
                <div className="headerText">
                    <h3 className="headerTitle">
                        Target
               </h3>
                    <p className="headerSubttle">
                        Cham Towers, Plot 12 Nkruma Rd, Kampala, UG
               </p>
                </div>
                <ul className="navItems">
                    <li>
                        <img src="/images/shop_icon.png" alt="" />
                        <span>Bag</span>
                    </li>

                    <li>
                        <img src="/images/nav_icon.png" alt="" />
                        <span>Orders</span>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default Header
