import React from 'react'
import SectionTitle from './SectionTitle'

function Bags() {
    return (
        <div className="bagContainer" id="appItem">
            <div>
                <SectionTitle
                    titleName='Bags'
                    numberOfItems='0'
                />
            </div>
            <div className="bagContent">
            <div className="bagIcon">
                <img src="./images/sad_icon.jpg" alt="" />
            </div>
            <div className="bagText">
                <h3>It's empty here</h3>
                <p>Start shopping to add items to your bag</p>
            </div>
            </div>
            
        </div>
    )
}

export default Bags
