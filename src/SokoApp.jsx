import React from 'react'
import MenuList from './Components/Menu'
import Bag from './Components/Bags'
import ItemsList from './Components/ItemsList'
import Header from './Components/Header'
import Footer from './Components/Footer'

import './sokoapp.css'

function SokoApp() {
    return (
        <div className='appComps'>
            <Header />
            <div className="appContent">
                <MenuList />
                <ItemsList />
                <Bag />
            </div>
            <Footer />
        </div>
    )
}

export default SokoApp
